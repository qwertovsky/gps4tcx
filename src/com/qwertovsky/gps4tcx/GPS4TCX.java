package com.qwertovsky.gps4tcx;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;

import com.garmin.schemas.tcx.v2.ActivityLapT;
import com.garmin.schemas.tcx.v2.ActivityListT;
import com.garmin.schemas.tcx.v2.ActivityT;
import com.garmin.schemas.tcx.v2.PositionT;
import com.garmin.schemas.tcx.v2.TrackT;
import com.garmin.schemas.tcx.v2.TrackpointT;
import com.garmin.schemas.tcx.v2.TrainingCenterDatabaseT;

public class GPS4TCX
{
	private ActivityListT activities;
	private TrainingCenterDatabaseT root = null;
	
	public GPS4TCX(File tcxFile) throws JAXBException, Exception
	{
		//read tcx file
		
		try
		{
			JAXBContext context = JAXBContext.newInstance(TrainingCenterDatabaseT.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			@SuppressWarnings("unchecked")
			JAXBElement<TrainingCenterDatabaseT> jaxbElement 
				= (JAXBElement<TrainingCenterDatabaseT>) unmarshaller
					.unmarshal(tcxFile);
			root = jaxbElement.getValue();
		} catch (JAXBException e)
		{
			String message = e.getMessage();
			// TODO log
			if(message == null || message.startsWith("unexpected element"))
				message = "may be it is not a TCX file";
			throw new JAXBException("Error parse TCX file: " + message);
		}
		 
		if(root == null)
			throw new Exception("Incorrect file format");
		
		//get activities
		setActivities(root.getActivities());
		
	}

	
	
	// -------------------------------------------
	public ByteArrayOutputStream addPathToTCX(File TCXFile, List<GPSPath> pathList)
	throws JAXBException, Exception
	{
		
		
		//get activity
		ActivityT activity = getActivities().getActivity().get(0);
		
		//add GPS data to tcx
		//loop for all trackpoints
		List<ActivityLapT> laps = activity.getLap();
		for(ActivityLapT lap:laps)
		{
			List<TrackT> tracks = lap.getTrack();
			for(TrackT track: tracks)
			{
				List<TrackpointT> trackpoints = track.getTrackpoint();
				for(int i = 0; i< trackpoints.size(); i++)
				{
					TrackpointT trackpoint = trackpoints.get(i);
					double trackpointDistance = 0;
					try
					{
						trackpointDistance = trackpoint.getDistanceMeters();
					} catch(NullPointerException e)
					{
						// get previous point
						if(i > 0)
						{
							TrackpointT prevTrackpoint = trackpoints.get(i - 1);
							trackpoint.setPosition(prevTrackpoint.getPosition());
							trackpoint.setAltitudeMeters(prevTrackpoint.getAltitudeMeters());
						}
						else
							trackpoint.setDistanceMeters(0.0);
						continue;
					}
					for(GPSPath path:pathList)
					{
						if(trackpointDistance >= path.getOffset()
							&& trackpointDistance <= path.getEndDistance())
						{
							// trackpoint on path
							GPSPoint point = path.getPoint(trackpointDistance);
							if(point == null)
								continue;
							trackpoint.setAltitudeMeters(point.getAltitude());
							PositionT position = new PositionT();
							position.setLatitudeDegrees(point.getLatitude());
							position.setLongitudeDegrees(point.getLongitude());
							trackpoint.setPosition(position);
							break;
						}
					}
				}
			}
		}
		
		// save
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try
		{
			JAXBContext context = JAXBContext.newInstance(TrainingCenterDatabaseT.class);
			Marshaller marshaller = context.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			JAXBElement<TrainingCenterDatabaseT> jaxbElement = new JAXBElement<TrainingCenterDatabaseT>(
					new QName("http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2", "TrainingCenterDatabase")
					, TrainingCenterDatabaseT.class, root);
			marshaller.marshal(jaxbElement, os);
			os.flush();
		} catch (JAXBException e)
		{
			throw new JAXBException("Error write TCX file: " + e.getMessage());
		}
		return os;
	}

	public void setActivities(ActivityListT activities)
	{
		this.activities = activities;
	}

	public ActivityListT getActivities()
	{
		return activities;
	}
}
