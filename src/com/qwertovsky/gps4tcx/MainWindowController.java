package com.qwertovsky.gps4tcx;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.ResourceBundle;

import netscape.javascript.JSObject;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.concurrent.WorkerStateEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.layout.Region;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.util.Callback;

import com.garmin.schemas.tcx.v2.ActivityListT;
import com.garmin.schemas.tcx.v2.ActivityT;


public class MainWindowController implements Initializable
{
	private String openDir = null;
	private String saveDir = null;
	private File tcxFile = null;
	private File tcxResultFile = null;
	private boolean elevationReady = false;
	private GPS4TCX gps4tcx;
	private ObservableList<GPSPath> pathList = FXCollections.observableArrayList();

	Stage window;

	@FXML
	private WebView webView;
	@FXML
	private TextField tcxFileTF;
	@FXML
	private Region webShadow;
	@FXML
	private Label activitySportL;
	@FXML
	private Label activityDateL;
	@FXML
	private Label activityDistanceL;
	@FXML
	private Label activityTimeL;
	@FXML
	private Label mapDistanceL;
	@FXML
	private TextField offsetTF;
	@FXML
	private TableView<GPSPath> pathTV;
	@FXML private TableColumn<GPSPath, Double> pathDistanceTC;
	@FXML private TableColumn<GPSPath, Double> pathOffsetTC;
	@FXML private ContextMenu pathListCM;
	@FXML
	private ProgressIndicator progressIndicator;
	@FXML
	private Button tcxFileB;
	@FXML
	private Button addPathB;
	@FXML
	private Button saveB;
	
	private WebEngine webEngine;

	// -------------------------------------------
	@Override
	public void initialize(URL arg0, ResourceBundle arg1)
	{
		webEngine = webView.getEngine();
		webEngine.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.20 Safari/535.1");
		try
		{
			webEngine.load(new File("resources/map.html").toURI().toURL()
					.toString());
		} catch (Exception e)
		{
			new MessageBox(window, "Error", "Can not load map. Check file exists - 'resources/map.html'."
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
		}
		
		//check available of Google Map
		try
		{
			URL u = new URL ( "https://www.google.com/jsapi");
			HttpURLConnection huc =  ( HttpURLConnection )  u.openConnection (); 
			huc.setRequestMethod ("GET"); 
			huc.connect () ; 
		}catch(Exception e)
		{
			new MessageBox(window, "Error", "Can not load Google Maps. Check internet connection or proxy settings."
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
		}
		mapDistanceL.setText("0;");
		offsetTF.setText("0");
		pathTV.setItems(pathList);
		addPathB.setDisable(true);
		
		// round values for display in table to 2 decimal places
		final int decimalPlaces = 2;
		pathDistanceTC.setCellValueFactory(new Callback<CellDataFeatures<GPSPath, Double>, ObservableValue<Double>>() {
		     public ObservableValue<Double> call(CellDataFeatures<GPSPath, Double> path) {
		         return new ReadOnlyObjectWrapper<Double>(
		        				 Math.round(path.getValue().getDistance() * (10*decimalPlaces)) / (10.*decimalPlaces)
		        		 );
		     }
		  });
		pathOffsetTC.setCellValueFactory(new Callback<CellDataFeatures<GPSPath, Double>, ObservableValue<Double>>() {
		     public ObservableValue<Double> call(CellDataFeatures<GPSPath, Double> path) {
		         return new ReadOnlyObjectWrapper<Double>(
		        				 Math.round(path.getValue().getOffset() * (10*decimalPlaces)) / (10.*decimalPlaces)
		        		 );
		     }
		  });
		
		// context menu for path table
		pathTV.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>()
				{
					@Override
					public void handle(ContextMenuEvent event)
					{
						int selected = ((TableView<?>)event.getSource()).getSelectionModel().getSelectedIndex();
						if(selected == -1)
							pathListCM.hide();
					}
				});
		
		progressIndicator.setVisible(false);
		progressIndicator.setProgress(-1.0);
	}

	// -------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void showFileOpenDialog()
	{
		FileChooser fileChooser = new FileChooser();
		ExtensionFilter supportFilter = new ExtensionFilter(
				"All supported file formats", "*.tcx");
		ExtensionFilter tcxFilter = new ExtensionFilter(
				"Training Center XML (*.tcx)", "*.tcx");
		ExtensionFilter allFilter = new ExtensionFilter("All (*.*)", "*");
		fileChooser.getExtensionFilters().addAll(supportFilter, tcxFilter,
				allFilter);
		File openDirFile = new File(".");
		if (openDir != null && openDir.length() > 0)
		{
			openDirFile = new File(openDir);
		}
		fileChooser.setInitialDirectory(openDirFile);

		try
		{
			tcxFile = fileChooser.showOpenDialog(window);
		} catch (IllegalArgumentException e)
		{
			openDir = null;
			fileChooser.setInitialDirectory(new File("."));
			tcxFile = fileChooser.showOpenDialog(window);
		}
		if (tcxFile == null)
			return;
		
		String selectedFileName = tcxFile.getName();
		String selectedFilePath = tcxFile.getAbsoluteFile().getPath();
		openDir = tcxFile.getParent();
		tcxFileTF.setText(selectedFilePath);
		
		//get information about activity
		Task<ActivityListT> readTCXTask = new Task<ActivityListT>(){
			protected ActivityListT call() throws Exception
			{
				gps4tcx = new GPS4TCX(tcxFile);
				return gps4tcx.getActivities();
			}
		};
		progressIndicator.visibleProperty().bind(readTCXTask.runningProperty());
		webShadow.visibleProperty().bind(readTCXTask.runningProperty());
		tcxFileB.disableProperty().bind(readTCXTask.runningProperty());
		addPathB.disableProperty().bind(readTCXTask.runningProperty());
		saveB.disableProperty().bind(readTCXTask.runningProperty());
		
		readTCXTask.setOnSucceeded(new EventHandler<WorkerStateEvent>()
		{
			
			@Override
			public void handle(WorkerStateEvent event)
			{
				
				ActivityListT activities = (ActivityListT) event.getSource().getValue();
				if(activities == null
						|| activities.getActivity() == null
						|| activities.getActivity().isEmpty())
				{
					new MessageBox(window, "Error", "TCX file is not contain activities"
							, MessageBox.DialogStyle.ERROR
							, MessageBox.DialogButtons.OK);
					webShadow.visibleProperty().unbind();
					webShadow.setVisible(true);
					tcxFileTF.clear();
					return;
				}
				//only one activity
				// TODO multisport
				ActivityT a = activities.getActivity().get(0);
				Activity activity = new Activity(a);
				activitySportL.setText(activity.getSport());
				activityDateL.setText(activity.getDate());
				activityDistanceL.setText(activity.getDistance());
				activityTimeL.setText(activity.getTime());
				
				//set this class to web document
				JSObject jdoc = (JSObject) webEngine.getDocument();
				jdoc.setMember("application", MainWindowController.this);
				
			}
		});
		readTCXTask.setOnFailed(new EventHandler<WorkerStateEvent>()
			{

				@Override
				public void handle(WorkerStateEvent event)
				{
					webShadow.visibleProperty().unbind();
					webShadow.setVisible(true);
					tcxFileTF.clear();
					
					MessageBox mb = new MessageBox(window, "Error"
							, event.getSource().getException().getMessage()
							,MessageBox.DialogStyle.ERROR, MessageBox.DialogButtons.OK);
				}
			});
		new Thread(readTCXTask).start();
		
	}

	// -------------------------------------------
	@FXML
	public void addPath()
	{
		// get offset
		String offsetS = offsetTF.getText();
		double offset;
		try
		{
			offset = Double.valueOf(offsetS);
		} catch(NumberFormatException e)
		{
			new MessageBox(window, "Error", "Bad format of offset. It must be a number."
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
			return;
		}
		
		// get distance
		// and check overlap other path
		String distanceS = mapDistanceL.getText();
		distanceS = distanceS.replaceAll(";", "");
		double distance;
		try
		{
			distance = Double.parseDouble(distanceS);
			for(GPSPath path: pathList)
			{
				if(path.getOffset() < offset + distance
						&& path.getEndDistance() > offset)
				{
					MessageBox mb = new MessageBox(window, "Warning", "Path overlap other path (" +
							path.getDistance() + " m, offset: " + path.getOffset() + ").\n" +
							"Add path?"
							, MessageBox.DialogStyle.WARNING
							, MessageBox.DialogButtons.OK_CANCEL);
					MessageBox.DialogResponse dr = mb.getResponse();
					if(dr == MessageBox.DialogResponse.CANCEL
							|| dr == MessageBox.DialogResponse.CLOSED)
					{
						return;
					}
				}
			}
		} catch(NumberFormatException e)
		{
			//if mapDistanceL is not in format '13456;'
			// TODO log 
			new MessageBox(window, "Error", "Illegal format of distance.\n Please contact author."
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
		}
		
		try
		{
			webEngine.executeScript("addPathToTCX(" + offsetS + ")");
		}catch(Exception e)
		{
			// TODO log
			new MessageBox(window, "Error", e.getMessage()
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
		}
	}

	protected boolean getElevationReady()
	{
		return elevationReady;
	}

	public void setElevationReady(boolean elevationReady)
	{
		this.elevationReady = elevationReady;
	}

	// -------------------------------------------
	public void addPathToTCX(Object offsetO, Object overviewPolyline, Object elevations)
	{
		if(offsetO instanceof String)
		{
			new MessageBox(window, "Error", "Bad offset"
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
			return;
		}
		double offset;
		if(offsetO instanceof Integer)
			offset = ((Integer) offsetO).doubleValue();
		else
			offset = (Double)offsetO;
		
		if (overviewPolyline instanceof String)
		{
			new MessageBox(window, "Error", "Can not get route: " + (String)overviewPolyline
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
			return;
		}
		if (elevations instanceof String)
		{
			new MessageBox(window, "Error", "Can not calculate elevations: " + (String)elevations
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
			return;
		}

		// get altitudes

		GPSPath path = new GPSPath(offset, (JSObject) overviewPolyline,
				(JSObject) elevations);
		pathList.add(path);
		Collections.sort(pathList, new Comparator<Object>()
		{

			@Override
			public int compare(Object o1, Object o2)
			{
				GPSPath p1 = (GPSPath)o1;
				GPSPath p2 = (GPSPath)o2;
				return compare(p1.getOffset(), p2.getOffset());
			}
		});
	}
	
	// -------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void deletePath()
	{
		if(pathTV.getSelectionModel().isEmpty())
			return;
		GPSPath path = pathTV.getSelectionModel().getSelectedItem();
		pathList.remove(path);
	}
	
	// -------------------------------------------
	@SuppressWarnings("unused")
	@FXML
	private void save()
	{
		FileChooser fileChooser = new FileChooser();
		ExtensionFilter supportFilter = new ExtensionFilter(
				"All supported file formats", "*.tcx");
		ExtensionFilter tcxFilter = new ExtensionFilter(
				"Training Center XML (*.tcx)", "*.tcx");
		ExtensionFilter allFilter = new ExtensionFilter("All (*.*)", "*");
		fileChooser.getExtensionFilters().addAll(supportFilter, tcxFilter,
				allFilter);
		File saveDirFile = new File(".");
		if (saveDir != null && saveDir.length() > 0)
		{
			saveDirFile = new File(saveDir);
		}
		fileChooser.setInitialDirectory(saveDirFile);

		try
		{
			tcxResultFile = fileChooser.showSaveDialog(window);
		} catch (IllegalArgumentException e)
		{
			saveDir = null;
			fileChooser.setInitialDirectory(new File("."));
			tcxResultFile = fileChooser.showOpenDialog(window);
		}
		if (tcxResultFile == null)
			return;
		
		Task<Void> saveTCXTask = new Task<Void>()
		{
			
			protected Void call() throws Exception
			{
				ByteArrayOutputStream os = null;
				FileOutputStream fos = null;
				try
				{
					os = gps4tcx.addPathToTCX(tcxFile, pathList);
					byte[] tcxBytes = os.toByteArray();
					fos = new FileOutputStream(tcxResultFile);
					fos.write(tcxBytes);
					fos.flush();
				} catch (Exception e)
				{
					throw e;
				} finally
				{
					if (os != null)
						try
						{
							os.close();
						} catch (IOException e){}
					if (fos != null)
						try
						{
							fos.close();
						} catch (IOException e){}
				}
				return null;
			}
		};
		
		progressIndicator.visibleProperty().bind(saveTCXTask.runningProperty());
		webShadow.visibleProperty().bind(saveTCXTask.runningProperty());
		tcxFileB.disableProperty().bind(saveTCXTask.runningProperty());
		addPathB.disableProperty().bind(saveTCXTask.runningProperty());
		addPathB.disableProperty().bind(saveTCXTask.runningProperty());
		saveB.disableProperty().bind(saveTCXTask.runningProperty());
		
		saveTCXTask.setOnFailed(new EventHandler<WorkerStateEvent>()
				{
					@Override
					public void handle(WorkerStateEvent event)
					{
						// TODO log
						String message = event.getSource().getException().getMessage();
						new MessageBox(window, "Error", message
								, MessageBox.DialogStyle.ERROR
								, MessageBox.DialogButtons.OK);
					}
				});
		new Thread(saveTCXTask).start();
	}
	
	// -------------------------------------------
	public void updateDistance(Object distance)
	{
		if(distance instanceof String)
		{
			new MessageBox(window, "Error", "Can not calculate distance: " + (String)distance
					, MessageBox.DialogStyle.ERROR
					, MessageBox.DialogButtons.OK);
			System.out.println("Update distance error: " + distance);
			return;
		}
		int dist;
		if(distance instanceof Integer)
			dist = (Integer) distance;
		else
			dist = ((Double)distance).intValue();
		mapDistanceL.setText(String.valueOf(dist) + ";");
	}
}
