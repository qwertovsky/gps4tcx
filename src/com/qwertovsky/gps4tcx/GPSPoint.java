package com.qwertovsky.gps4tcx;

public class GPSPoint
{
	private double latitude;
	private double longitude;
	private Double altitude = null;
	
	public GPSPoint(double latitude, double longitude)
	{
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	// -------------------------------------------
	public double getLatitude()
	{
		return latitude;
	}
	public void setLatitude(double lat)
	{
		this.latitude = lat;
	}
	
	// -------------------------------------------
	public double getLongitude()
	{
		return longitude;
	}
	public void setLongitude(double lng)
	{
		this.longitude = lng;
	}
	
	// -------------------------------------------
	public Double getAltitude()
	{
		return altitude;
	}
	public void setAltitude(double altitude)
	{
		this.altitude = altitude;
	}
}
