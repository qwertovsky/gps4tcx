package com.qwertovsky.gps4tcx;



import java.io.File;
import java.net.URL;
import java.util.Locale;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Main extends Application
{
	private ResourceBundle mainBundle;
	
	public static void main(String[] args)
	{
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception
	{
		System.getProperties().put("proxyHost", "10.58.5.11");
		System.getProperties().put("proxyPort", "8080");
		
		URL fxmlURL = new File("resources/MainWindow.xml").toURI().toURL();
		FXMLLoader loader = new FXMLLoader(fxmlURL);
		try
		{
			mainBundle = ResourceBundle.getBundle("MainWindow");
		} catch(Exception e)
		{
			Locale locale = new Locale("en","GB");
			mainBundle = ResourceBundle.getBundle("MainWindow", locale);
		}
		loader.setResources(mainBundle);
		Scene scene = (Scene)loader.load();
		MainWindowController controller = loader.getController();
		controller.window = stage;
		Rectangle2D r = Screen.getPrimary().getVisualBounds();
		stage.setHeight(r.getHeight());
		stage.setWidth(r.getWidth());
		stage.setScene(scene);
		stage.setMinHeight(400);
		stage.setMinWidth(600);
		stage.setTitle("GPS4TXC");
		stage.show();
	}

}
