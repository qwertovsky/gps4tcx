package com.qwertovsky.gps4tcx;

public class PathPoint extends GPSPoint
{
	private Double distance;
	
	public PathPoint(double latitude, double longitude)
	{
		super(latitude, longitude);
	}
	
	// -------------------------------------------
	public void setDistance(double distance)
	{
		this.distance = distance;
	}

	public Double getDistance()
	{
		return distance;
	}
}
