package com.qwertovsky.gps4tcx;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import netscape.javascript.JSObject;

import org.xml.sax.InputSource;




public class GPSPath
{
	private List<PathPoint> pathPoints = new ArrayList<PathPoint>();
	private double offset = 0;
	private XMLGregorianCalendar startTime;
	
	public GPSPath(List<GPSPoint> wayPoints)
	{
		this(getOverviewPolyline(wayPoints));
	}
	
	// -------------------------------------------
	private GPSPath(String overviewPolyline)
	{
		if(overviewPolyline == null)
			return;
		
		
		// parse polyline
		List<PathPoint> points = getPathPoints(overviewPolyline);
		this.pathPoints = points;
		
		//add points with altitude
		points = getAltitudePoints(overviewPolyline);
		pathPoints.addAll(points);
		
		//sort by distance
		Collections.sort(pathPoints, new Comparator<PathPoint>()
		{
			@Override
			public int compare(PathPoint p1, PathPoint p2)
			{
				return p1.getDistance().compareTo(p2.getDistance());
			}
		});
	}
	
	// -------------------------------------------
	public GPSPath(double offset, JSObject overviewPolyline, JSObject elevations)
	throws ClassCastException
	{
		setOffset(offset);
		
		List<PathPoint> points = new ArrayList<PathPoint>();
		
		for(int i = 0; ;i++)
		{
			Object latLng = (Object)overviewPolyline.getSlot(i);
			if(latLng instanceof String)
				break;
			double lat;
			Object latObject = ((JSObject)latLng).call("lat");
			if(latObject instanceof Integer)
				lat = ((Integer)latObject).doubleValue();
			else
				lat = (Double)latObject;
			
			double lng;
			Object lngObject = (Double)((JSObject)latLng).call("lng");
			if(lngObject instanceof Integer)
				lng = ((Integer)lngObject).doubleValue();
			else
				lng = (Double)lngObject;
			PathPoint p = new PathPoint(lat, lng);
			
			if (points.size() == 0)
			{
				p.setDistance(offset);
			}
			else
			{
				PathPoint prevPoint = points.get(points.size() - 1);
				double distance = getDistance(prevPoint, p);
				p.setDistance(prevPoint.getDistance() + distance);
			}
			points.add(p);
		}
		this.pathPoints = points;
		
		// add points with altitude
		points = new ArrayList<PathPoint>();
		for(int i = 0; ; i++)
		{
			Object elevation = (Object)elevations.getSlot(i);
			if(elevation instanceof String)
				break;
			JSObject latLng = (JSObject)((JSObject)elevation).getMember("location");
			double lat;
			Object latObject = ((JSObject)latLng).call("lat");
			if(latObject instanceof Integer)
				lat = ((Integer)latObject).doubleValue();
			else
				lat = (Double)latObject;
			
			double lng;
			Object lngObject = (Double)((JSObject)latLng).call("lng");
			if(lngObject instanceof Integer)
				lng = ((Integer)lngObject).doubleValue();
			else
				lng = (Double)lngObject;
			
			double alt;
			Object altObject = ((JSObject)elevation).getMember("elevation");
			if(altObject instanceof Integer)
				alt = ((Integer)altObject).doubleValue();
			else
				alt = (Double)altObject;
			
			PathPoint p = new PathPoint(lat, lng);
			p.setAltitude(alt);
			
			if (points.size() == 0)
			{
				p.setDistance(offset);
			}
			else
			{
				PathPoint prevPoint = points.get(points.size() - 1);
				double distance = getDistance(prevPoint, p);
				p.setDistance(prevPoint.getDistance() + distance);
			}
			points.add(p);
		}
		this.pathPoints.addAll(points);
		
		//sort by distance
		Collections.sort(pathPoints, new Comparator<PathPoint>()
		{
			@Override
			public int compare(PathPoint p1, PathPoint p2)
			{
				return p1.getDistance().compareTo(p2.getDistance());
			}
		});
	}
	
	// -------------------------------------------
	private List<PathPoint> getPathPoints(String overviewPolyline)
	{
		List<PathPoint> points = new ArrayList<PathPoint>();
		int len = overviewPolyline.length();
		double lat = 0;
		double lng = 0;
		int index = 0;
		while (index < len)
		{
			
			int code;
			int shift = 0;
			int result = 0;
			
			do
			{
				code = overviewPolyline.charAt(index++) - 63;
				result |= (code & 0x1f) << shift;
				shift += 5;
			} while (code >= 0x20);
			int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lat += dlat / 100000.;

			shift = 0;
			result = 0;
			do
			{
				code = overviewPolyline.charAt(index++) - 63;
				result |= (code & 0x1f) << shift;
				shift += 5;
			} while (code >= 0x20);
			int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
			lng += dlng / 100000.;

			PathPoint p = new PathPoint(lat, lng);
			

			// get distance
			if (points.size() == 0)
			{
				p.setDistance(0);
			}
			else
			{
				PathPoint prevPoint = points.get(points.size() - 1);
				double distance = getDistance(prevPoint, p);
				p.setDistance(prevPoint.getDistance() + distance);
			}
			points.add(p);
		}
		return points;
	}
	
	// -------------------------------------------
	private List<PathPoint> getAltitudePoints(String overviewPolyline)
	{
		List<PathPoint> points = new ArrayList<PathPoint>();
		// TODO create request to Google
		
		//get points
		File altFile = new File("altitudes.xml");
		InputStream altIS = null;
		
		try
		{
			altIS = new FileInputStream(altFile);
			XMLInputFactory factory = XMLInputFactory.newInstance();
			XMLEventReader reader = factory.createXMLEventReader(altIS);
			PathPoint point = null;
			while(reader.hasNext())
			{
				XMLEvent event = reader.nextEvent();
				if(event.isStartElement())
				{
					StartElement element = event.asStartElement();
					if(element.getName().getLocalPart().equals("status"))
					{
						String status = reader.getElementText();
						System.out.println("Elevation response status: " + status);
						if(!status.equals("OK"))
						{
							// TODO error
							break;
						}
					}
					if(element.getName().getLocalPart().equals("result"))
					{
						point = new PathPoint(0,0);
					}
					if(element.getName().getLocalPart().equals("lat"))
					{
						String latString = reader.getElementText();
						double lat = Double.valueOf(latString);
						point.setLatitude(lat);
					}
					if(element.getName().getLocalPart().equals("lng"))
					{
						String lngString = reader.getElementText();
						double lng = Double.valueOf(lngString);
						point.setLongitude(lng);
					}
					if(element.getName().getLocalPart().equals("elevation"))
					{
						String elevationString = reader.getElementText();
						double elevation = Double.valueOf(elevationString);
						point.setAltitude(elevation);
					}
				}
				if(event.isEndElement())
				{
					EndElement element = event.asEndElement();
					if(element.getName().getLocalPart().equals("result"))
					{
						if(points.size() == 0)
							point.setDistance(0);
						else
						{
							PathPoint prevPoint = points.get(points.size() - 1);
							double distance = getDistance(prevPoint, point);
							point.setDistance(prevPoint.getDistance() + distance);
						}
						points.add(point);
					}
				}
			}
			reader.close();
		} catch (FileNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(altIS != null)
			try
			{
				altIS.close();
			} catch (IOException e)
			{
				
			}
		return points;
	}
	
	// -------------------------------------------
	private double getDistance(GPSPoint p1, GPSPoint p2)
	{
		double lat2 = p2.getLatitude();
		double lng2 = p2.getLongitude();
		double lat1 = p1.getLatitude();
		double lng1 = p1.getLongitude();
		double dlat = lat2 - lat1;
		double dlng = lng2 - lng1;
		// 6367000
		double radius = Math
				.sqrt((Math.pow(
						Math.pow(6384400, 2)
								* Math.cos(lat2 * Math.PI / 180),
						2) + Math.pow(
						Math.pow(6352800, 2)
								* Math.sin(lat2 * Math.PI / 180),
						2))
						/ (Math.pow(
								6384400 * Math.cos(lat2 * Math.PI
										/ 180), 2) + Math.pow(
								6352800 * Math.sin(lat2 * Math.PI
										/ 180), 2)));

		double dist1 = radius
				* 2
				* Math.asin(Math.sqrt(Math.pow(
						Math.sin(dlat * Math.PI / 180 / 2), 2)
						+ Math.cos(lat1
								* Math.PI / 180)
						* Math.cos(lat2 * Math.PI / 180)
						* Math.pow(Math.sin(dlng * Math.PI / 180
								/ 2), 2)));

		double dist2 = 2
				* radius
				* Math.PI
				/ 360
				* Math.acos(Math.sin(lat1
						* Math.PI / 180)
						* Math.sin(lat2  * Math.PI / 180)
						+ Math.cos(lat1
								* Math.PI / 180)
						* Math.cos(lat2  * Math.PI / 180)
						* Math.cos(dlng * Math.PI / 180))
				/ Math.PI * 180;

		double distance = (dist1 + dist2) / 2;
		return distance;
	}
	
	// -------------------------------------------
	private static String getOverviewPolyline(List<GPSPoint> wayPoints)
	{
		// create request to Google Map API
		
		// get response
		File routeFile = new File("route.xml");
		XPathFactory factory = XPathFactory.newInstance();
	    XPath xpath = factory.newXPath();
	    
		// get overview polyline
	    
	    String overviewPolyline = null;
		try
		{
			//check status
			String path = "/DirectionsResponse/status";
			String status =  xpath.evaluate(path
					, new InputSource(new FileInputStream(routeFile)));
			if(status.equals("OK"))
			{
				path = "/DirectionsResponse/route/overview_polyline/points/text()";
				overviewPolyline = xpath.evaluate(path
						, new InputSource(new FileInputStream(routeFile)));
				System.out.println(overviewPolyline);
				return overviewPolyline;
			}
		} catch (XPathExpressionException e)
		{
			e.printStackTrace();
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	// -------------------------------------------
	public List<PathPoint> getPathPoints()
	{
		return pathPoints;
	}
	
	// -------------------------------------------
	public GPSPoint getPoint(double distance)
	{
		boolean firstPathPoint = false;
		for(int i = 0; i < pathPoints.size();i++)
		{
			PathPoint pathPoint =  pathPoints.get(i);
			double pathPointDistance = pathPoint.getDistance();
			
			if(pathPoint.getAltitude() == null && !firstPathPoint)
				firstPathPoint = true;
			
			if(pathPoint.getAltitude() != null)
				continue;
			
			if(!firstPathPoint && pathPointDistance > distance)
			{
				return null;
			}
			
			if(pathPointDistance >= distance && firstPathPoint)
			{
				GPSPoint point = null;
				double xlat;
				double xlng;
				// get previous path point
				PathPoint prevPathPoint = null;
				for(int j = 1; i - j >= 0; j++)
				{
					if(pathPoints.get(i-j).getAltitude() == null)
					{
						prevPathPoint = pathPoints.get(i-j);
						break;
					}
				}
				
				if(prevPathPoint == null)
				{
					xlat = pathPoint.getLatitude();
					xlng = pathPoint.getLongitude();
				}
				else
				{
					xlat = prevPathPoint.getLatitude()
						+ (distance - prevPathPoint.getDistance())
						* ((pathPoint.getLatitude() - prevPathPoint.getLatitude())
								/ (pathPoint.getDistance() - prevPathPoint.getDistance())
						  );
					xlng = prevPathPoint.getLongitude()
					+ (distance - prevPathPoint.getDistance())
					* ((pathPoint.getLongitude() - prevPathPoint.getLongitude())
							/ (pathPoint.getDistance() - prevPathPoint.getDistance())
					  );
				}
				point = new GPSPoint(xlat, xlng);
				
				//get next altitude point
				PathPoint altPoint = null;
				for(int j = 1; i + j < pathPoints.size(); j++)
				{
					if(pathPoints.get(i+j).getAltitude() != null)
					{
						altPoint = pathPoints.get(i+j);
						break;
					}
				}
				
				// get previous altitude point
				PathPoint prevAltPoint = null;
				for(int j = 1; i - j >= 0; j++)
				{
					if(pathPoints.get(i-j).getAltitude() != null)
					{
						prevAltPoint = pathPoints.get(i-j);
						break;
					}
				}
				
				if(altPoint == null || prevAltPoint == null)
				{
					if(altPoint != null)
						point.setAltitude(altPoint.getAltitude());
					if(prevAltPoint != null)
						point.setAltitude(prevAltPoint.getAltitude());
				}
				else
				{
					double xalt = prevAltPoint.getAltitude()
					+ (distance - prevAltPoint.getDistance())
					* ( (altPoint.getAltitude() - prevAltPoint.getAltitude())
							/ (altPoint.getDistance() - prevAltPoint.getDistance())
					   );
					point.setAltitude(xalt);
				}
				return point;
			}
		}
		return null;
	}
	
	// -------------------------------------------
	public void setOffset(double offset)
	{
		this.offset = offset;
	}

	public double getOffset()
	{
		return offset;
	}

	public void setStartTime(XMLGregorianCalendar startTime)
	{
		this.startTime = startTime;
	}

	public XMLGregorianCalendar getStartTime()
	{
		return startTime;
	}

	public double getDistance()
	{
		return getEndDistance() - offset;
	}
	
	public double getEndDistance()
	{
		return pathPoints.get(pathPoints.size() - 1).getDistance();
	}
}
