package com.qwertovsky.gps4tcx;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;

public class MessageBox
{
	private DialogResponse response;
	private Stage stage;
	
	public MessageBox(Stage parentWindow, String title, String message
			, DialogStyle style, DialogButtons buttons)
	{
		stage = new Stage();
		stage.initOwner(parentWindow);
		if(parentWindow != null)
			stage.getIcons().setAll(parentWindow.getIcons());
		stage.initModality(Modality.APPLICATION_MODAL);
		stage.initStyle(StageStyle.UTILITY);
		stage.setTitle(title);
		stage.setOnCloseRequest(new EventHandler<WindowEvent>()
				{
					public void handle(WindowEvent event)
					{
						MessageBox.this.response = DialogResponse.CLOSED;
					}
				});
		
		BorderPane root = new BorderPane();
		
		Scene scene = new Scene(root, 300, 150);
		stage.setScene(scene);
		
		root.setTop(createTopPane(style));
		root.setCenter(createCenterPane(message));
		root.setBottom(createBottomPane(buttons));
		
		if(parentWindow != null)
		{
			stage.setX(parentWindow.getX()+(parentWindow.getWidth()-scene.getWidth()) / 2);
			stage.setY(parentWindow.getY() + (parentWindow.getHeight()-scene.getHeight()) / 2);
		}
		stage.showAndWait();
	}
	
	// -------------------------------------------
	private Node createBottomPane(DialogButtons buttons)
	{
		HBox buttonBox = new HBox();
		buttonBox.setPadding(new Insets(5,10,5,10));
		buttonBox.setAlignment(Pos.CENTER_RIGHT);
		buttonBox.setSpacing(15);
		// create buttons
		if(buttons == DialogButtons.OK)
		{
			buttonBox.getChildren().addAll(createButton("Ok", DialogResponse.OK, true, false));
		}
		if(buttons == DialogButtons.OK_CANCEL)
		{
			buttonBox.getChildren().addAll(createButton("Ok", DialogResponse.OK, true, false)
					, createButton("Cancel", DialogResponse.CANCEL, false, true));
		}
		return buttonBox;
	}
	
	// -------------------------------------------
	private Node createCenterPane(String message)
	{
		VBox pane = new VBox();
		TextArea messageTA = new TextArea(message);
		messageTA.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		messageTA.setWrapText(true);
		messageTA.setEditable(false);
		messageTA.setMinSize(0, 0);
		messageTA.setStyle("-fx-background-color: #ccc, #f5f5f5;" +
				" -fx-background-insets: 2 0 0 0, 1 0 1 0;" );
		pane.getChildren().add(messageTA);
		return pane;
	}

	// -------------------------------------------
	private Node createTopPane(DialogStyle style)
	{
		BorderPane pane = new BorderPane();
		pane.setStyle("-fx-background-color:  #aaa," +
        "radial-gradient(center 50% -40%, radius 200%, #ddd 45%, #ccc 60%);" +
        "-fx-background-insets: 0,0 0 1 0;");
		pane.setPadding(new Insets(5,10,5,5));
		Label imageLabel = new Label();
		Image image = null;
		if(style == DialogStyle.ERROR)
			image = new Image("file:resources/dialog-error.png");
		if(style == DialogStyle.WARNING)
			image = new Image("file:resources/dialog-warning.png");
		if(style == DialogStyle.INFO)
			image = new Image("file:resources/dialog-information.png");
		if(style == DialogStyle.QUESTION)
			image = new Image("file:resources/dialog-question.png");
		imageLabel.setGraphic(new ImageView(image));
		
		pane.setRight(imageLabel);
		return pane;
	}

	// -------------------------------------------
	private Button createButton(String text, final DialogResponse response, boolean isDefault, boolean isCancel)
	{
		Button button = new Button(text);
		button.setDefaultButton(isDefault);
		button.setCancelButton(isCancel);
		button.setOnAction(new EventHandler<ActionEvent>()
				{
					@Override
					public	void handle(ActionEvent event)
					{
						MessageBox.this.setResponse(response);
						stage.close();
					}
				});
		return button;
	}
	
	// -------------------------------------------
	public void setResponse(DialogResponse response)
	{
		this.response = response;
	}

	public DialogResponse getResponse()
	{
		return response;
	}

	// -------------------------------------------
	enum DialogStyle
	{
		ERROR, INFO, WARNING, QUESTION
	}
	
	enum DialogButtons
	{
		OK, OK_CANCEL, YES_NO, YES_NO_CANCEL
	}
	
	enum DialogResponse
	{
		OK, CANCEL, YES, NO, CLOSED
	}
}
