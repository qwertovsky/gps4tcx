package com.qwertovsky.gps4tcx;

import java.util.GregorianCalendar;

import javax.xml.datatype.XMLGregorianCalendar;

import com.garmin.schemas.tcx.v2.ActivityLapT;
import com.garmin.schemas.tcx.v2.ActivityT;

public class Activity
{
	private String id;
	private String date;
	private String distance;
	private String time;
	private String sport;
	
	// -------------------------------------------
	protected Activity(ActivityT a)
	{
		XMLGregorianCalendar id = a.getId(); 
		setId(id.toString());
		GregorianCalendar gc = id.toGregorianCalendar();
		setDate(String.format("%1$tF %1$tT",gc.getTime()));
		setSport(a.getSport().value());
		Double distance = new Double(0);
		int totalTimeSeconds = 0;
		for(ActivityLapT lap:a.getLap())
		{
			distance += lap.getDistanceMeters();
			totalTimeSeconds += lap.getTotalTimeSeconds();
		}
		setDistance(distance.intValue() + " m");
		setTime(String.format("%1$tT", new GregorianCalendar(0, 0, 0, 0, 0, totalTimeSeconds)));
		
	}
	// -------------------------------------------
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getDate()
	{
		return date;
	}
	public void setDate(String date)
	{
		this.date = date;
	}
	public String getDistance()
	{
		return distance;
	}
	public void setDistance(String distance)
	{
		this.distance = distance;
	}
	public String getTime()
	{
		return time;
	}
	public void setTime(String time)
	{
		this.time = time;
	}
	public void setSport(String sport)
	{
		this.sport = sport;
	}
	public String getSport()
	{
		return sport;
	}
}
